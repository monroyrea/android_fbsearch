package com.usc.monroyre.facebooksearchapp;

import android.content.Context;
import android.media.Image;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.usc.monroyre.facebooksearchapp.ResultstActivity.context;

/**
 * Created by alejandro on 4/10/17.
 */

public class FacebookObjectAdapter extends ArrayAdapter<FacebookObject> {
    public FacebookObjectAdapter(Context context, ArrayList<FacebookObject> fb) {
        super(context, 0, fb);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FacebookObject fb = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.facebook_object, parent, false);
        }

        ImageView fbImage = (ImageView) convertView.findViewById(R.id.ImageFacebookObject);
        TextView fbName = (TextView) convertView.findViewById(R.id.NameFacebookObject);
        ImageView fbFavorite = (ImageView) convertView.findViewById(R.id.FavoriteFacebookObject);
        ImageView fbDetail = (ImageView) convertView.findViewById(R.id.DetailFacebookObject);

        if(context != null)
            Picasso.with(context).load(fb.getFbImage()).into(fbImage);
        fbName.setText(fb.getFbName());
        fbFavorite.setImageResource(fb.getFbFavorite());
        fbDetail.setImageResource(fb.getFbDetail());

        return convertView;
    }


}

