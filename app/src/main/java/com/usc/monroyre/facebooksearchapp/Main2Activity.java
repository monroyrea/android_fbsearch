package com.usc.monroyre.facebooksearchapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    static final int MY_LOCATION_REQUEST_CODE = 1;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    static LocationManager lm;
    static double latitude;
    static double longitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_LOCATION_REQUEST_CODE);

         latitude = 0;
         longitude = 0;

        sharedPreferences = this.getSharedPreferences("SETTINGS", this.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Context context = this;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button clearButton = (Button) this.findViewById(R.id.ButtonClear);
        Button searchButton = (Button) this.findViewById(R.id.ButtonSearch);
        final EditText keywordEditText = (EditText) this.findViewById(R.id.EditTextKeyword);

        CharSequence text = "Please enter a keyword!!";
        int duration = Toast.LENGTH_SHORT;

        final Toast toast = Toast.makeText(this, text, duration);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keywordEditText.setText("");
            }
        });

        searchButton.setOnClickListener(new  View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(keywordEditText.getText().toString().isEmpty() || keywordEditText.getText().toString().trim().length() == 0){
                    toast.show();
                }
                else{
                    Intent intent = new Intent(context, ResultstActivity.class);
                    String keyword = keywordEditText.getText().toString();
                    intent.putExtra("FBKEYWORD", keyword);
                    editor.putString("TAB", "1");
                    editor.apply();
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("ENTERED","E");
        switch (requestCode) {
            case MY_LOCATION_REQUEST_CODE:
                if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                    Location loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (loc == null) {
                        LocationListener locationListener = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {

                                // getting location of user
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                Log.d("LAT", String.valueOf(latitude));
                                Log.d("Lon", String.valueOf(longitude));
                                //do something with Lat and Lng
                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                                //when user enables the GPS setting, this method is triggered.
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                                //when no provider is available in this case GPS provider, trigger your gpsDialog here.
                            }
                        };

                        //update location every 10sec in 500m radius with both provider GPS and Network.

                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                    }
                    else {
                        //do something with last known location.
                        // getting location of user
                        latitude = loc.getLatitude();
                        longitude = loc.getLongitude();
                        Log.d("LAT", String.valueOf(latitude));
                        Log.d("Lon", String.valueOf(longitude));
                    }
               } else {
                    // Permission Denied
                    Toast.makeText(this, "LOCATION DENIED", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            Intent intent = new Intent(this, AboutMe.class);
            startActivity(intent);
        } else if (id == R.id.nav_favorites) {
            Intent intent = new Intent(this, FavoritesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_home) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
