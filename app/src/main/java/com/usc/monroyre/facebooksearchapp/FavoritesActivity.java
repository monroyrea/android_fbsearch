package com.usc.monroyre.facebooksearchapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.camera2.params.Face;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FavoritesActivity extends AppCompatActivity {

    static Context context;
    static SharedPreferences myFavorites;
    static SharedPreferences sharedPreferences1;
    static SharedPreferences.Editor editor;

    public FavoritesActivity() {
        context = this;
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myFavorites = this.getSharedPreferences("FAVORITES", Context.MODE_PRIVATE);
        sharedPreferences1 = context.getSharedPreferences("SETTINGS", context.MODE_PRIVATE);
        editor = myFavorites.edit();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.setTabTextColors(Color.BLACK, Color.BLACK);
        tabLayout.setBackgroundColor(Color.WHITE);
        tabLayout.getTabAt(0).setIcon(R.drawable.users);
        tabLayout.getTabAt(0).setText("Users");
        tabLayout.getTabAt(1).setIcon(R.drawable.pages);
        tabLayout.getTabAt(1).setText("Pages");
        tabLayout.getTabAt(2).setIcon(R.drawable.events);
        tabLayout.getTabAt(2).setText("Events");
        tabLayout.getTabAt(3).setIcon(R.drawable.places);
        tabLayout.getTabAt(3).setText("Places");
        tabLayout.getTabAt(4).setIcon(R.drawable.groups);
        tabLayout.getTabAt(4).setText("Groups");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        ArrayList<FacebookObject> favoritesArray;
        FacebookObjectAdapter favoritesAdapter;

        Map<String, ?> allEntries;
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
            favoritesArray = new ArrayList<>();
            favoritesAdapter = new FacebookObjectAdapter(context, favoritesArray);

            allEntries  = myFavorites.getAll();
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.favorites_listview);

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                listView.setAdapter(favoritesAdapter);
                getFavorites(allEntries, favoritesAdapter,1);
                favoritesAdapter.notifyDataSetChanged();
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
                listView.setAdapter(favoritesAdapter);
                getFavorites(allEntries, favoritesAdapter,2);
                favoritesAdapter.notifyDataSetChanged();
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 3) {
                listView.setAdapter(favoritesAdapter);
                getFavorites(allEntries, favoritesAdapter,3);
                favoritesAdapter.notifyDataSetChanged();
                Log.d("waaaa",favoritesArray.toString().toString());
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 4) {
                listView.setAdapter(favoritesAdapter);
                getFavorites(allEntries, favoritesAdapter,4);
                favoritesAdapter.notifyDataSetChanged();
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 5) {
                listView.setAdapter(favoritesAdapter);
                getFavorites(allEntries, favoritesAdapter,5);
                favoritesAdapter.notifyDataSetChanged();
            }

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);
                    FacebookObject fb = (FacebookObject) parent.getAdapter().getItem(position);
                    intent.putExtra("FBDETAILS_NAME", fb.getFbName());
                    intent.putExtra("FBDETAILS_IMG", fb.getFbImage());
                    intent.putExtra("FBDETAILS_ID", fb.getId());

                    startActivity(intent);
                }
            });
            return rootView;
        }

        public void getFavorites(Map<String, ?> allEntries, FacebookObjectAdapter favoritesAdapter, int n){
            favoritesAdapter.clear();
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                Set<String> set = myFavorites.getStringSet(entry.getKey(), null);
                if(set != null){
                    String[] a = set.toArray(new String[set.size()]);
                    java.util.Arrays.sort(a);

                    String cat = a[0].substring(1, a[0].length());
                    String id = a[1].substring(1, a[1].length());
                    String name = a[2].substring(1, a[2].length());
                    String url = a[3].substring(1, a[3].length());

                    if (Integer.parseInt(cat) == n) {
                        favoritesAdapter.add(new FacebookObject(url, name, R.drawable.details, R.drawable.details, id, getActivity()));
                    }
                }
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
                case 4:
                    return "SECTION 5";
            }
            return null;
        }
    }
}
