package com.usc.monroyre.facebooksearchapp;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.usc.monroyre.facebooksearchapp.ResultstActivity.context;

/**
 * Created by alejandro on 4/12/17.
 */

public class PostObjectAdapter extends ArrayAdapter<PostObject> {
    SimpleDateFormat input = new SimpleDateFormat("MMM dd,yyyy hh:mm a");

    public PostObjectAdapter(Context context, ArrayList<PostObject> objects) {
        super(context, 0, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PostObject fb = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.post_object, parent, false);
        }

        ImageView postImage = (ImageView) convertView.findViewById(R.id.post_Image);
        TextView postName = (TextView) convertView.findViewById(R.id.post_name);
        TextView postDate = (TextView) convertView.findViewById(R.id.post_date);
        TextView postText = (TextView) convertView.findViewById(R.id.post_text);

        Picasso.with(context).load(fb.getUrl()).into(postImage);
        postName.setText(fb.getName());
        postDate.setText(fb.getDate());
        postText.setText(fb.getText());

        return convertView;
    }
}
