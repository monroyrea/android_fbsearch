package com.usc.monroyre.facebooksearchapp;

import com.facebook.FacebookSdk;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DetailsActivity extends AppCompatActivity {
    static Context context;
    static String idFB;
    static boolean isFav;
    static String nameFB;
    static String imgFB;
    static String catFB;
    static RequestQueue mRequestQueue;
    SharedPreferences myFavorites;
    SharedPreferences.Editor editor;

    public DetailsActivity(){
        context = this;
        isFav = false;
    }
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myFavorites = this.getSharedPreferences("FAVORITES", Context.MODE_PRIVATE);
        editor = myFavorites.edit();

        Set fav = myFavorites.getStringSet(idFB, null);
        if (fav != null){
            isFav = true;
        }

        setContentView(R.layout.activity_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        Intent intent = getIntent();
        idFB = intent.getStringExtra("FBDETAILS_ID");
        nameFB = intent.getStringExtra("FBDETAILS_NAME");
        imgFB = intent.getStringExtra("FBDETAILS_IMG");
        catFB = intent.getStringExtra("FBDETAILS_TYPE");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.setTabTextColors(Color.BLACK, Color.BLACK);
        tabLayout.setBackgroundColor(Color.WHITE);
        tabLayout.getTabAt(0).setIcon(R.drawable.albums);
        tabLayout.getTabAt(0).setText("Albums");
        tabLayout.getTabAt(1).setIcon(R.drawable.posts);
        tabLayout.getTabAt(1).setText("Posts");

        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Intent intent = getIntent();
        String id = intent.getStringExtra("FBDETAILS_ID");
        Set fav = myFavorites.getStringSet(id, null);
        if (fav == null){
            getMenuInflater().inflate(R.menu.menu_details, menu);
            return true;

        }else
        {
            getMenuInflater().inflate(R.menu.menu_details2,menu);
            return true;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if (id == R.id.action_favorites) {
            Set<String> set = new HashSet<>();
            set.add("1" + catFB);
            set.add("2" + idFB);
            set.add("3" + nameFB);
            set.add("4" + imgFB);
            editor.putStringSet(idFB, set);
            editor.apply();
            Toast toast = Toast.makeText(context, "Added to Favorites", Toast.LENGTH_SHORT);
            toast.show();
            return true;
        }   else if(id== R.id.action_favorites_remove) {
            editor.remove(idFB);
            editor.apply();
            Toast toast = Toast.makeText(context, "Removed From Favorites", Toast.LENGTH_SHORT);
            toast.show();
            return true;

        }
        else if (id == R.id.action_share){
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("http://facebook.com/" + idFB))
                    .setContentTitle(nameFB)
                    .setContentDescription(
                            "FB SEARCH FROM USC CSCI571...")
                    .build();

            ShareDialog.show(this, linkContent);


        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        static String token = "EAAJdx7RriqkBAPt1ElQyI0hISnRhm6xNwdDqNcQ9RaXQ9qogIgNdqvwwxwB6l34Ee8QsHaJBozEWKOh7SvXBlx3jSrBRA6KBpILhtSbE2ZCZAzuDbh9SRBZCsc0SIJYx7MJjLxOqDMtg4MKwJSBfUwAuj7gZB0wZD";
        static String myPicture;
        ArrayList<PostObject> array;
        PostObjectAdapter adapter;
        ExpandableListView expandableListView;
        ExpandableListAdapter expandableListAdapter;
        List<String> listDataHeader;
        HashMap<String, List<String>> listHash;
        String detailsQuery = "https://graph.facebook.com/v2.8/"+idFB+"?fields=albums.limit(5){name,photos.limit(2){name,picture.width(700).height(700)}},posts.limit(5)&access_token="+token;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
            array = new ArrayList<>();
            adapter = new PostObjectAdapter(context, array);

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = null;
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 2)
            {
                rootView = inflater.inflate(R.layout.fragment_details, container, false);
                ListView listView = (ListView) rootView.findViewById(R.id.section_label);
                TextView textView = (TextView) rootView.findViewById(R.id.text_post);
                array.clear();
                listView.setAdapter(adapter);

                getJSONRequest(detailsQuery, mRequestQueue, adapter, textView);
                adapter.notifyDataSetChanged();
            }

            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1){
                rootView = inflater.inflate(R.layout.fragment_details2, container, false);
                TextView textView = (TextView) rootView.findViewById(R.id.text_album);
                listDataHeader = new ArrayList<>();
                listHash = new HashMap<>();
                listDataHeader.clear();
                listHash.clear();

                getJSONRequestAlbum(detailsQuery,mRequestQueue,listDataHeader,listHash,textView);


                expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandable_list_view);
                expandableListAdapter = new com.usc.monroyre.facebooksearchapp.ExpandableListAdapter(context, listDataHeader, listHash);
                expandableListView.setAdapter(expandableListAdapter);
            }
            return rootView;
        }

        public void  getJSONRequest(String url, RequestQueue mRequestQueue, final PostObjectAdapter adapter, final TextView textView){

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            int j =0;
                            try {
                                textView.setVisibility(View.GONE);
                                textView.setText("");
                                JSONObject json = response.getJSONObject("posts");
                                JSONArray jsonArray = json.getJSONArray("data");
                                j = jsonArray.length();
                                for(int i = 0; i<jsonArray.length(); i++)
                                {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    adapter.add(new PostObject(imgFB, nameFB, obj.getString("created_time"), obj.getString("message")));
                                }
                            } catch (JSONException e) {
                                if(j==0){
                                    textView.setVisibility(View.VISIBLE);
                                    textView.setText("No posts available to display");}
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText("No posts available to display");
                        }
                    });

            mRequestQueue.add(jsObjRequest);
        }

        public void  getJSONRequestAlbum(String url, final RequestQueue mRequestQueue, final List<String> listDataHeader, final HashMap<String, List<String>> listHash, final TextView textView){

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            int size = 0 ;
                            try {
                                textView.setVisibility(View.GONE);
                                textView.setText("");
                                JSONArray json = response.getJSONObject("albums").getJSONArray("data");
                                size=json.length();
                                for(int i = 0; i<json.length(); i++)
                                {
                                    JSONObject obj = json.getJSONObject(i);
                                    if(obj.length() != 0) {
                                        listDataHeader.add(obj.getString("name"));

                                        JSONArray array = obj.getJSONObject("photos").getJSONArray("data");
                                        List<String> a = new ArrayList<>();
                                        for(int j = 0 ; j< array.length(); j++){
                                            JSONObject object = array.getJSONObject(j);
                                            String id = object.getString("id");
                                            getHdPicture(id, mRequestQueue, a,listHash, j);
                                        }
                                        listHash.put(listDataHeader.get(i),a);
                                    }
                                }
                            } catch (JSONException e) {
                                if(size==0){
                                    textView.setVisibility(View.VISIBLE);
                                    textView.setText("No Albums available to display");}
                            }


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            textView.setVisibility(View.VISIBLE);
                            textView.setText("No Albums available to display");

                        }
                    });

            mRequestQueue.add(jsObjRequest);
        }

        public void getHdPicture(String id, RequestQueue mRequestQueue, final List<String> a, final HashMap<String, List<String>> listHash , final int j ){
            String url = "https://graph.facebook.com/v2.8/" + id + "/picture?access_token=" +token + "&redirect=false";
            JsonObjectRequest jsObjRequest = new JsonObjectRequest

                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                myPicture = response.getJSONObject("data").getString("url");
                                a.add(myPicture);
                            } catch (JSONException e) {
                            }


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            a.add("a");
                        }
                    });
            mRequestQueue.add(jsObjRequest);
        }
        /*        listDataHeader.add("THis is Expandable ListView");
                listDataHeader.add("Android");
                listDataHeader.add("Google");

        List<String> a = new ArrayList<>();
                a.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");
                a.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");

        List<String> b = new ArrayList<>();
                b.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");
                b.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");

        List<String> c = new ArrayList<>();
                c.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");
                c.add("http://cs-server.usc.edu:45678/hw/hw9/images/android/albums.png");

                listHash.put(listDataHeader.get(0), a);
                listHash.put(listDataHeader.get(1), b);
                listHash.put(listDataHeader.get(2), c);*/
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
            }
            return null;
        }
    }
}
