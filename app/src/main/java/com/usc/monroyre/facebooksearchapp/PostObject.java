package com.usc.monroyre.facebooksearchapp;

/**
 * Created by alejandro on 4/12/17.
 */

public class PostObject {
    private String url;
    private String name;
    private String date;
    private String text;

    public PostObject(String url, String name, String date, String text){
        this.url = url;
        this.name=name;
        this.date = date;
        this.text=text;
    }

    public String getUrl()
    {
        return url;
    }

    public String getName(){
        return name;
    }

    public String getDate(){
        return date;
    }

    public String getText(){
        return text;
    }
}
