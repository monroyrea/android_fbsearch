package com.usc.monroyre.facebooksearchapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.jar.Manifest;


public class ResultstActivity extends AppCompatActivity {
    static Context context;
    static String keyword;
    static RequestQueue mRequestQueue;
    static TabLayout tabLayout;

    public ResultstActivity(){
        context = this;
    }
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private static ViewPager mViewPager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences1 = this.getSharedPreferences("SETTINGS", context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultst);
        keyword = getIntent().getStringExtra("FBKEYWORD");


        if(keyword == null){
            keyword = sharedPreferences1.getString("KEYWORD", "NO");
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabTextColors(Color.BLACK, Color.BLACK);
        tabLayout.setBackgroundColor(Color.WHITE);
        tabLayout.getTabAt(0).setIcon(R.drawable.users);
        tabLayout.getTabAt(0).setText("Users");
        tabLayout.getTabAt(1).setIcon(R.drawable.pages);
        tabLayout.getTabAt(1).setText("Pages");
        tabLayout.getTabAt(2).setIcon(R.drawable.events);
        tabLayout.getTabAt(2).setText("Events");
        tabLayout.getTabAt(3).setIcon(R.drawable.places);
        tabLayout.getTabAt(3).setText("Places");
        tabLayout.getTabAt(4).setIcon(R.drawable.groups);
        tabLayout.getTabAt(4).setText("Groups");
        String tab = sharedPreferences1.getString("TAB", "1");
        tabLayout.getTabAt(Integer.parseInt(tab) -1).select();

        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //adapter.notifyDataSetChanged();



        // getJSONRequest("https://graph.facebook.com/v2.8/search?q=usc&type=event&fields=id,name,picture.width(700).height(700)&access_token=" + token);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int mPage;

        //Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //double longitude = 0;
        //double latitude = 0 ;

        static String token = "EAAJdx7RriqkBAPt1ElQyI0hISnRhm6xNwdDqNcQ9RaXQ9qogIgNdqvwwxwB6l34Ee8QsHaJBozEWKOh7SvXBlx3jSrBRA6KBpILhtSbE2ZCZAzuDbh9SRBZCsc0SIJYx7MJjLxOqDMtg4MKwJSBfUwAuj7gZB0wZD";
        ArrayList<FacebookObject> array;
        FacebookObjectAdapter adapter;

                String word = URLEncoder.encode(keyword, "UTF-8");


        String linkBase = "https://graph.facebook.com/v2.8/search?q=" + word;
        String usersQuery = linkBase +  "&type=user&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;
        String pagesQuery = linkBase +  "&type=page&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;
        String eventsQuery = linkBase +  "&type=event&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;
        //String placesQuery = linkBase +  "&type=place&center=" + latitude + "," + longitude + "&distance1000&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;
        String placesQuery = linkBase +  "&type=place&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;

        String groupsQuery = linkBase +  "&type=group&limit=10&fields=id,name,picture.width(300).height(300)&access_token=" + token;
        String curTab;
        static String pagingNU, pagingPU;
        static String pagingNP, pagingPP;
        static String pagingNE, pagingPE;
        static String pagingNPL, pagingPPL;
        static String pagingNG, pagingPG;


        public PlaceholderFragment() throws UnsupportedEncodingException {
            array = new ArrayList<>();
            adapter = new FacebookObjectAdapter(context, array);
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = null;
            try {
                fragment = new PlaceholderFragment();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstatnceState){
            super.onCreate(savedInstatnceState);
            Bundle args = getArguments();
            mPage = args.getInt(ARG_SECTION_NUMBER);
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);

            if(isVisibleToUser)
            {

            }
        }



        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_resultst, container, false);
            final ListView listView = (ListView) rootView.findViewById(R.id.section_label);
            final Button buttonNext = (Button) rootView.findViewById(R.id.nextButton);
            final Button buttonPrev = (Button) rootView.findViewById(R.id.previousButton);

            final int tab = mPage;
                if (tab == 1){
                    array.clear();
                    listView.setAdapter(adapter);
                    getJSONRequest(usersQuery, context, mRequestQueue,adapter, buttonNext, buttonPrev, tab);
                    adapter.notifyDataSetChanged();
                    curTab ="1";
                }else  if (tab == 2){
                    array.clear();
                    listView.setAdapter(adapter);
                    getJSONRequest(pagesQuery, context, mRequestQueue,adapter, buttonNext, buttonPrev,tab);
                    adapter.notifyDataSetChanged();
                    curTab ="2";
                }else if (tab == 3){
                    array.clear();
                    listView.setAdapter(adapter);
                    getJSONRequest(eventsQuery, context, mRequestQueue, adapter, buttonNext, buttonPrev,tab);
                    adapter.notifyDataSetChanged();
                    curTab ="3";
                 }else if (tab == 4){
                    array.clear();
                    listView.setAdapter(adapter);
                    Log.d("AAAAAQUERY", placesQuery);
                    Log.d("PLACES QUERY",placesQuery);
                    getJSONRequest(placesQuery, context, mRequestQueue,adapter, buttonNext, buttonPrev,tab);
                    adapter.notifyDataSetChanged();
                    curTab ="4";
                }else if (tab == 5){
                    array.clear();
                    listView.setAdapter(adapter);
                    getJSONRequest(groupsQuery, context, mRequestQueue,adapter, buttonNext, buttonPrev,tab);
                    adapter.notifyDataSetChanged();
                    curTab ="5";
                }

                buttonNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        array.clear();
                        listView.setAdapter(adapter);
                        switch (mViewPager.getCurrentItem()) {
                            case 0:
                                getJSONRequest(pagingNU, context, mRequestQueue, adapter, buttonNext, buttonPrev, 1);
                                break;
                            case 1:
                                getJSONRequest(pagingNP, context, mRequestQueue, adapter, buttonNext, buttonPrev, 2);
                                break;
                            case 2:
                                getJSONRequest(pagingNE, context, mRequestQueue, adapter, buttonNext, buttonPrev, 3);
                                break;
                            case 3:
                                getJSONRequest(pagingNPL, context, mRequestQueue, adapter, buttonNext, buttonPrev, 4);
                                break;
                            case 4:
                                getJSONRequest(pagingNG, context, mRequestQueue, adapter, buttonNext, buttonPrev, 5);
                                break;
                            default:
                                break;
                        }
                        adapter.notifyDataSetChanged();
                    }
                });

                buttonPrev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        array.clear();
                        listView.setAdapter(adapter);
                        switch (mViewPager.getCurrentItem()) {
                            case 0:
                                getJSONRequest(pagingPU, context, mRequestQueue, adapter, buttonNext, buttonPrev, 1);
                                break;
                            case 1:
                                getJSONRequest(pagingPP, context, mRequestQueue, adapter, buttonNext, buttonPrev, 2);
                                break;
                            case 2:
                                getJSONRequest(pagingPE, context, mRequestQueue, adapter, buttonNext, buttonPrev, 3);
                                break;
                            case 3:
                                getJSONRequest(pagingPPL, context, mRequestQueue, adapter, buttonNext, buttonPrev, 4);
                                break;
                            case 4:
                                getJSONRequest(pagingPG, context, mRequestQueue, adapter, buttonNext, buttonPrev, 5);
                                break;
                            default:
                                break;
                        }
                        adapter.notifyDataSetChanged();
                    }
                });


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);
                    FacebookObject fb = (FacebookObject) parent.getAdapter().getItem(position);
                    intent.putExtra("FBDETAILS_NAME", fb.getFbName());
                    intent.putExtra("FBDETAILS_IMG", fb.getFbImage());
                    intent.putExtra("FBDETAILS_ID", fb.getId());
                    intent.putExtra("FBDETAILS_TYPE", curTab.toString());

                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("SETTINGS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("KEYWORD", keyword);
                    editor.putString("TAB", curTab);
                    editor.apply();

                    startActivity(intent);
                }
            });
            return rootView;
        }

        public void getJSONRequest(String url, final Context context, final RequestQueue mRequestQueue, final FacebookObjectAdapter adapter, final Button b1, final Button b2, final int tab){
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray json = response.getJSONArray("data");
                                for(int i = 0; i<json.length(); i++)
                                {
                                    JSONObject obj = json.getJSONObject(i);
                                    if (obj.length()>0)
                                    adapter.add(new FacebookObject(obj.getJSONObject("picture").getJSONObject("data").getString("url"), obj.getString("name"), R.drawable.details, R.drawable.details, obj.getString("id"), getActivity()));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                String pagingN = null;
                                switch (tab) {
                                    case 1:
                                        pagingNU = response.getJSONObject("paging").getString("next");
                                        pagingN = pagingNU;
                                        break;
                                    case 2:
                                        pagingNP = response.getJSONObject("paging").getString("next");
                                        pagingN = pagingNP;
                                        break;
                                    case 3:
                                        pagingNE = response.getJSONObject("paging").getString("next");
                                        pagingN = pagingNE;
                                        break;
                                    case 4:
                                        pagingNPL = response.getJSONObject("paging").getString("next");
                                        pagingN = pagingNPL;
                                        break;
                                    case 5:
                                        pagingNG = response.getJSONObject("paging").getString("next");
                                        pagingN = pagingNG;
                                        break;
                                    default:
                                        break;
                                }
                                testPaging(pagingN, context, mRequestQueue,b1);
                                } catch (JSONException e1) {

                                b1.setEnabled(false);
                            }

                            try{
                                switch (tab){
                                    case 1:
                                        pagingPU = response.getJSONObject("paging").getString("previous");
                                        break;
                                    case 2:
                                        pagingPP = response.getJSONObject("paging").getString("previous");
                                        break;
                                    case 3:
                                        pagingPE = response.getJSONObject("paging").getString("previous");
                                        break;
                                    case 4:
                                        pagingPPL = response.getJSONObject("paging").getString("previous");
                                        break;
                                    case 5:
                                        pagingPG = response.getJSONObject("paging").getString("previous");
                                        break;
                                    default:
                                        break;
                                }

                                b2.setEnabled(true);
                            } catch (JSONException e1) {
                                b2.setEnabled(false);
                            }


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {


                        }
                    });

            mRequestQueue.add(jsObjRequest);
        }

        public int testPaging(String url, final Context context, RequestQueue mRequestQueue, final Button b1){
            final int[] responseNull = {0};
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray json = response.getJSONArray("data");
                                if(json.length() <1)
                                    b1.setEnabled(false);
                                else
                                    b1.setEnabled(true);
                               // b1.setEnabled(true);
                            } catch (JSONException e) {
                                  b1.setEnabled(false);
                               // e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {


                        }
                    });
            mRequestQueue.add(jsObjRequest);
            return responseNull[0];
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
                case 4:
                    return "SECTION 5";
            }
            return null;
        }
    }
}
