package com.usc.monroyre.facebooksearchapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by alejandro on 4/10/17.
 */

public class FacebookObject{
    SharedPreferences myFavorite;
    private  String fbImage;
    private String fbName;
    private int fbFavorite;
    private int fbDetail;
    private String id;

    public FacebookObject(String fbImage, String fbName, int fbFavorite, int fbDetail, String id, Activity activity){
        myFavorite = activity.getSharedPreferences("FAVORITES", Context.MODE_PRIVATE);
        this.fbImage = fbImage;
        this.fbName = fbName;
        this.fbFavorite = fbFavorite;
        this.fbDetail = fbDetail;
        this.id = id;
    }

    public String getFbImage()
    {
        return fbImage;
    }

    public String getFbName()
    {
        return fbName;
    }

    public int getFbFavorite() {
        Set<String> fav = myFavorite.getStringSet(id, null);
        if(fav == null)
            return R.drawable.favorites_off;
        return R.drawable.favorites_on;
    }


    public int getFbDetail(){
        return R.drawable.details;
    }

    public String getId(){ return id;}
}

